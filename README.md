# Talana test

This project contains source code and supporting files for a serverless application that you can deploy with the SAM CLI. It includes the following files and folders.

To build and deploy your application for the first time, run the following in your shell:

```bash
killall Docker && open /Applications/Docker.app
docker-compose build
docker-compose run web django-admin startproject <name_project> .
```
if port 5432 is used:
```bash
sudo lsof -i :5432
sudo kill -9 <pid>
```
## Run Docker
```bash
docker-compose up
```
make migrations:

```bash
docker-compose run web python manage.py makemigrations
docker-compose run web python manage.py migrate
```

Create app:

```bash
docker-compose run web django-admin startapp api
```
Create super user:
```bash
docker-compose run web python manage.py createsuperuser
```

rebuild:
```bash
docker-compose up -d --build
```
The SAM CLI reads the application template to determine the API's routes and the functions that they invoke. The `Events` property on each function's definition includes the route and method for each path.
