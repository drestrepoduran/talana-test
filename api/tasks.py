from celery import task, shared_task
from django.core.mail import EmailMessage
from django.http import HttpResponse
import random
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from app.settings import DEFAULT_DOMAIN

@task(name="send_email")
def send_email(token, data):
    mail_subject = 'Activate your account.'
    message = render_to_string('active_email.html', {
        'username': data["email"],
        'domain': DEFAULT_DOMAIN,
        'uid': data["id"],
        'token': token,
    })

    email = EmailMessage(mail_subject, message, to=[data["email"]])
    email.send()
    print("OK")


