from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from api.models import User
from rest_framework.validators import UniqueTogetherValidator

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', "email"]
        validators = [
            UniqueTogetherValidator(
                queryset=User.objects.all(),
                fields=['email'],
                message= 'This email exits, please insert a new email'
            )
        ]

