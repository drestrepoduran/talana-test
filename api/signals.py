from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import User
from django.contrib.auth.tokens import default_token_generator
from .tasks import send_email

@receiver(post_save, sender=User)
def post_save_user(sender, instance, created, *args, **kwargs):
    if instance and created:
        token = default_token_generator.make_token(instance)
        user = {
            'id': instance.id,
            'email': instance.email
        }
        send_email.delay(token, user)
