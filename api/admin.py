from django.contrib import admin
from .models import User

class PublisherAdmin(admin.ModelAdmin):
    list_display = ("name")

admin.site.register(User)
