from django.urls import path
from api.views import SignUpAPI, LotteryAPI, activate



urlpatterns = [
    path('lottery/', LotteryAPI.as_view()),
    path('sign_up/', SignUpAPI.as_view()),
    path('activate/<id>/<str:token>/', activate, name='activate'),

]
