from django.shortcuts import render
from rest_framework import generics, viewsets, status, permissions
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from .models import User
from .serializers import UserSerializer
from .tasks import send_email
import random
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.db import connection

class LotteryAPI(APIView):
    """
    View to define the winner of the lottery

    Returns:
        return winner user
    """
    def get(self, request, format=None):
        users = User.objects.filter(is_active=True).values('id', 'username', 'first_name', 'last_name', 'email').cache()
        users_quantity = users.count()
        winner_number = random.randrange(1, users_quantity)
        winner_user = users[winner_number]
        serializer = UserSerializer(winner_user)
        return Response(serializer.data)


class SignUpAPI(APIView):
    """
    View to create new user

    Parameters:  json
        {
        email (str):        Email user.
        first_name (str):   Firstname user
        last_name (str):    Lastname user ,
        password (str):     Password registered by user
        username (str):     Username registered by user
        }
    Returns:
          return message to verify email
    """
    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def activate(request, id, token):
    """
    View to active user with token sent in email
    Returns:
          return message with status
    """
    try:
        user = User.objects.get(id=id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        return Response({"message": "Thank you for your email confirmation. Now you can login your account."})
    else:
        return Response({"message": 'Activation link is invalid!'})


